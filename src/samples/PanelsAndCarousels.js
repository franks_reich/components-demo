import React from 'react';

import {
  ComponentTitle, RowContainer, Text, ItemTitle, ApplicationPanel,
  ComponentPanel, ComponentSubPanel, ItemPanel, ItemSubPanel
} from 'franks-reich-components';


export const PanelsAndCarousels = () => {
  const content =
    <React.Fragment>
      <RowContainer>
        <Text>
          Some text for the component panel. This could be more. Any content can
          be added. Tables, text, you name it.
        </Text>
      </RowContainer>
      <RowContainer>
        <ItemTitle>An Item Header</ItemTitle>
      </RowContainer>
      <RowContainer>
        <Text>And some more text...</Text>
      </RowContainer>
    </React.Fragment>;
  return (
    <React.Fragment>
      <RowContainer>
        <ComponentTitle>Panels and Carousels</ComponentTitle>
      </RowContainer>
      <RowContainer>
        <ApplicationPanel
          title="Application Panel"
          content={ content } />
      </RowContainer>
      <RowContainer>
        <ComponentPanel
          title="Component Panel"
          content={ content } />
      </RowContainer>
      <RowContainer>
        <ComponentSubPanel
          title="Component Sub Panel"
          content={ content } />
      </RowContainer>
      <RowContainer>
        <ItemPanel
          title="Item Panel"
          content={ content } />
      </RowContainer>
      <RowContainer>
        <ItemSubPanel
          title="Item Sub Panel"
          content={ content } />
      </RowContainer>
    </React.Fragment>
  );
};
