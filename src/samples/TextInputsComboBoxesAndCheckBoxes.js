import React from 'react';

import {
  ApplicationTitleInput, ComponentTitleInput, ComponentSubTitleInput,
  ItemTitleInput, ItemSubTitleInput, TextInput, RowContainer, Spacer,
  ComponentTitle, PaddingContainer, TextInputGrid
} from 'franks-reich-components';


const inputGridData = [
  {
    label: 'Label 1',
    value: 'Value 1',
    onChange: () => {}
  },
  {
    label: 'Label 2',
    value: 'Value 2',
    onChange: () => {}
  },
  {
    label: 'Label 3',
    value: 'Value 3',
    onChange: () => {}
  },
  {
    label: 'Label 4',
    value: 'Value 4',
    onChange: () => {}
  },
  {
    label: 'Label 5',
    value: 'Value 5',
    onChange: () => {}
  },
  {
    label: 'Label 6',
    value: 'Value 6',
    onChange: () => {}
  },
  {
    label: 'Label 7',
    value: 'Value 7',
    onChange: () => {}
  },
  {
    label: 'Label 8',
    value: 'Value 8',
    onChange: () => {}
  }
];

export class TextInputsComboBoxesAndCheckBoxes extends React.Component {
  render() {
    return (
      <div>
        <RowContainer>
          <ComponentTitle>Input Fields</ComponentTitle>
        </RowContainer>
        <RowContainer>
          <ApplicationTitleInput
            label='Application Title Input'
            value='The Input Value' />
        </RowContainer>
        <RowContainer>
          <ComponentTitleInput
            label='Component Title Input'
            value='The Input Value' />
        </RowContainer>
        <RowContainer>
          <ComponentSubTitleInput
            label='Component Sub-Title Input'
            value='The Input Value' />
        </RowContainer>
        <RowContainer>
          <ItemTitleInput
            label='Item Title Input'
            value='The Input Value' />
        </RowContainer>
        <RowContainer>
          <ItemSubTitleInput
            label='Item Sub-Title Input'
            value='The Input Value' />
        </RowContainer>
        <RowContainer>
          <TextInput
            label='Text Input'
            value='The Input Value' />
        </RowContainer>
        <Spacer height='1em' />
        <PaddingContainer>
          <ComponentTitle>Input Grid</ComponentTitle>
        </PaddingContainer>
        <RowContainer>
          <TextInputGrid columnCount={ 3 } data={ inputGridData } />
        </RowContainer>
      </div>
    );
  }
}
