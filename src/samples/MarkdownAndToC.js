import React from 'react';

import {
  ComponentTitle, RowContainer, Markdown, MarkdownTableOfContents,
  PageRowContainer
} from 'franks-reich-components';


const markDownText = `
<a id="firstheader"></a>

# First Header

Some text.

<a id="secondheader"></a>

## Second Header

<a id="alist"></a>

### A list

 - First list item
 - Second list item
 - Third list item

<a id="anorderedlist"></a>

#### An ordered list

 1. First item
 2. Second item
 3. Third item
 
 
<a id="thirdheader"></a>

### 1.1.1\\. Third Header

Some more text and a table

Header 1 | Header 2 | Header 3
---|---|---
Field 1 | Field 2 | Field 3
Field 1 | Field 2 | Field 3
Field 1 | Field 2 | Field 3

\`\`\`C++
int main(int argc, char *argv[]) {
    printf("Hello World");
}
\`\`\`
`;

export const MarkdownAndToC = (props) => {
  return (
    <React.Fragment>
      <RowContainer>
        <ComponentTitle>Markdown and ToC</ComponentTitle>
      </RowContainer>
      <PageRowContainer>
        <Markdown
          source={ markDownText }
          escapeHtml={ false } />
        <MarkdownTableOfContents
          source={ markDownText }
          baseUrl="/markdown_and_toc"
          linkType="Router" />
      </PageRowContainer>
    </React.Fragment>
  );
};
