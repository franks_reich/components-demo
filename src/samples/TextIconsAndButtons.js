import React from 'react';

import {
  ApplicationTitle, ApplicationHighlightTitle, ComponentTitle, ComponentHighlightTitle,
  ComponentSubTitle, ComponentHighlightSubTitle, ItemSubTitle, ItemTitle, Text,
  ItemHighlightTitle, ItemHighlightSubTitle, HighlightText, BoldText
} from 'franks-reich-components';

import {
  ButtonApplicationTitle, ButtonComponentTitle, ButtonComponentSubTitle, ButtonItemTitle,
  ButtonItemSubTitle, ButtonText
} from 'franks-reich-components';

import {
  RowContainer, Spacer
} from 'franks-reich-components';

import {
  IconApplicationTitle, IconComponentTitle, IconComponentSubTitle, IconItemTitle,
  IconItemSubTitle, IconText
} from 'franks-reich-components';


export const TextIconsAndButtons = () => (
  <React.Fragment>
    <RowContainer>
      <ComponentTitle>Buttons, Icons and Texts in a Row Container</ComponentTitle>
    </RowContainer>
    <RowContainer>
      <ApplicationTitle>Application Title</ApplicationTitle>
      <Spacer width='1em' />
      <ButtonApplicationTitle iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonApplicationTitle iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonApplicationTitle text='Button Without Icon' />
      <Spacer />
      <IconApplicationTitle className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <ApplicationHighlightTitle>23</ApplicationHighlightTitle>
    </RowContainer>

    <RowContainer>
      <ComponentTitle>Component Title</ComponentTitle>
      <Spacer width='1em' />
      <ButtonComponentTitle iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonComponentTitle iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonComponentTitle text='Button Without Icon' />
      <Spacer />
      <IconComponentTitle className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <ComponentHighlightTitle>23</ComponentHighlightTitle>
    </RowContainer>

    <RowContainer>
      <ComponentSubTitle>Component Sub-Title</ComponentSubTitle>
      <Spacer width='1em' />
      <ButtonComponentSubTitle iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonComponentSubTitle iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonComponentSubTitle text='Button Without Icon' />
      <Spacer />
      <IconComponentSubTitle className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <ComponentHighlightSubTitle>23</ComponentHighlightSubTitle>
    </RowContainer>

    <RowContainer>
      <ItemTitle>Item Title</ItemTitle>
      <Spacer width='1em' />
      <ButtonItemTitle iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonItemTitle iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonItemTitle text='Button Without Icon' />
      <Spacer />
      <IconItemTitle className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <ItemHighlightTitle>23</ItemHighlightTitle>
    </RowContainer>

    <RowContainer>
      <ItemSubTitle>Item Sub-Title</ItemSubTitle>
      <Spacer width='1em' />
      <ButtonItemSubTitle iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonItemSubTitle iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonItemSubTitle text='Button Without Icon' />
      <Spacer />
      <IconItemSubTitle className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <ItemHighlightSubTitle>23</ItemHighlightSubTitle>
    </RowContainer>

    <RowContainer>
      <Text>Item Title</Text>
      <Spacer width='1em' />
      <ButtonText iconClass='fas fa-plus-circle' text='Button With Icon' />
      <Spacer width='1em' />
      <ButtonText iconClass='fas fa-plus-circle' />
      <Spacer width='1em' />
      <ButtonText text='Button Without Icon' />
      <Spacer />
      <IconText className='fas fa-thumbs-up' />
      <Spacer width='1em' />
      <HighlightText>23</HighlightText>
      <Spacer width='1em' />
      <BoldText>23</BoldText>
    </RowContainer>
  </React.Fragment>
);
