import React from 'react';

import {
  TileGroup, ComponentTitle, RowContainer
} from 'franks-reich-components';


export const TileGroups = () => {
  const title = "Tile Group 1";
  const tiles = [
      {
        type: "Button",
        title: "Button Tile Title 1",
        subTitle: "Button Tile Sub-Title 1",
        onClick: () => { console.log("Clicked button 1") }
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 1",
        subTitle: "Navigation Tile Sub-Title 1 - Router",
        url: "/",
        navType: "Router"
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 2",
        subTitle: "Navigation Tile Sub-Title 2 - Html",
        url: "/",
        navType: "Html"
      },
      {
        type: "Button",
        title: "Button Tile Title 2",
        subTitle: "Button Tile Sub-Title 2",
        onClick: () => { console.log("Clicked button 2") }
      }
    ];

  const tileGroupTwoTitle = "Tile Group 2";
  const tileGroupTwo = [
    {
      type: "Button",
      title: "Button Tile Title 1",
      subTitle: "Button Tile Sub-Title 1",
      onClick: () => { console.log("Clicked button 1") }
    },
    {
      type: "Navigation",
      title: "Navigation Tile Title 1",
      subTitle: "Navigation Tile Sub-Title 1 - Router",
      url: "/",
      navType: "Router"
    },
    {
      type: "Navigation",
      title: "Navigation Tile Title 2",
      subTitle: "Navigation Tile Sub-Title 2 - Html",
      url: "/",
      navType: "Html"
    }
  ];
  return (
    <React.Fragment>
      <RowContainer>
        <ComponentTitle>Tile Groups and Tiles</ComponentTitle>
      </RowContainer>
      <TileGroup
        title={ title }
        tiles={ tiles } />
      <TileGroup
        title={ tileGroupTwoTitle }
        tiles={ tileGroupTwo } />
    </React.Fragment>
  );
};
