import React from 'react';

import {
  ComponentTitle, RowContainer, ButtonComponentSubTitle, BusyOverlay,
  OverlayDialog, OverlayTileMenu, OverlayPane, ComponentSubTitle,
  PaddingContainer, Text, Spacer, OverlayMenu
} from 'franks-reich-components';


const tileGroups = [
  {
    title: "Tile Group 1",
    tiles: [
      {
        type: "Button",
        title: "Button Tile Title 1",
        subTitle: "Button Tile Sub-Title 1",
        onClick: () => { console.log("Clicked button 1") }
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 1",
        subTitle: "Navigation Tile Sub-Title 1 - Router",
        url: "/",
        navType: "Router"
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 2",
        subTitle: "Navigation Tile Sub-Title 2 - Html",
        url: "/",
        navType: "Html"
      },
      {
        type: "Button",
        title: "Button Tile Title 2",
        subTitle: "Button Tile Sub-Title 2",
        onClick: () => { console.log("Clicked button 2") }
      }
    ]
  },
  {
    title: "Tile Group 2",
    tiles: [
      {
        type: "Button",
        title: "Button Tile Title 1",
        subTitle: "Button Tile Sub-Title 1",
        onClick: () => { console.log("Clicked button 1") }
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 1",
        subTitle: "Navigation Tile Sub-Title 1 - Router",
        url: "/",
        navType: "Router"
      },
      {
        type: "Navigation",
        title: "Navigation Tile Title 2",
        subTitle: "Navigation Tile Sub-Title 2 - Html",
        url: "/",
        navType: "Html"
      }
    ]
  }
];

const overlayPaneContent = (
  <React.Fragment>
    <PaddingContainer>
      <ComponentSubTitle>Overlay Pane</ComponentSubTitle>
    </PaddingContainer>
    <RowContainer>
      <Text>
        The overlay pane can be used to display information on a
        pane on top of the normal page.
      </Text>
    </RowContainer>
    <Spacer height="1em" />
  </React.Fragment>
);

export class OverlaysAndDialogs extends React.Component {
  constructor() {
    super();
    this.state = {
      busyOverlay: false,
      dialogOverlay: false,
      overlayTileMenu: false
    };

    this.showBusyOverlay = this.showBusyOverlay.bind(this);
    this.showOverlayDialog = this.showOverlayDialog.bind(this);
    this.confirmOverlayDialog = this.confirmOverlayDialog.bind(this);
    this.cancelOverlayDialog = this.cancelOverlayDialog.bind(this);
    this.cancelNavigation = this.cancelNavigation.bind(this);
    this.showOverlayTileMenu = this.showOverlayTileMenu.bind(this);
  }

  render() {
    const busyOverlay = this.state.busyOverlay ? <BusyOverlay /> : null;
    const leftButton = {
      text: "Okay",
      iconClass: "fas fa-check",
      onClick: this.confirmOverlayDialog
    };
    const rightButton = {
      text: "Cancel",
      iconClass: "fas fa-times",
      onClick: this.cancelOverlayDialog
    };
    const overlayDialog = this.state.dialogOverlay ?
      <OverlayDialog
        leftButton={ leftButton }
        rightButton={ rightButton }
        backgroundOnClick={ this.cancelOverlayDialog }
        title="Dialog Title"
        text="Example text for the dialog, with multiple line to show the maximum width." /> : null;
    const overlayTileMenu = this.state.overlayTileMenu ?
      <OverlayTileMenu
        onCancelNavigation={ this.cancelNavigation }
        tileGroups={ tileGroups } /> : null;
    const overlayPane = <OverlayPane
      position={{
        top: "200px",
        left: "300px",
        width: "300px"
      }}
      content={ overlayPaneContent }
    />;
    const overlayMenu = <OverlayMenu
      button={ ButtonComponentSubTitle }
      items={
        [
          {
            type: "Button",
            title: "Sub Menu Button 1",
            onClick: () => { console.log("Clicked Sub-Menu Button 1") }
          },
          {
            type: "Button",
            title: "Sub Menu Button 2",
            onClick: () => { console.log("Clicked Sub-Menu Button 2") }
          },
          {
            type: "Navigation",
            title: "Sub Menu Navigation 1",
            navType: "Router",
            url: "/"
          }
        ]}
      text="Show Overlay Menu" />;
    return (
      <React.Fragment>
        { busyOverlay }
        { overlayDialog }
        { overlayTileMenu }
        { overlayPane }
        <RowContainer>
          <ComponentTitle>Overlays and Dialogs</ComponentTitle>
        </RowContainer>
        <RowContainer>
          <ButtonComponentSubTitle
            text="Show Busy Overlay"
            onClick={ this.showBusyOverlay } />
        </RowContainer>
        <RowContainer>
          <ButtonComponentSubTitle
            text="Show Overlay Dialog"
            onClick={ this.showOverlayDialog } />
        </RowContainer>
        <RowContainer>
          <ButtonComponentSubTitle
            text="Show Overlay Tile Menu"
            onClick={ this.showOverlayTileMenu } />
        </RowContainer>
        <RowContainer>
          { overlayMenu }
        </RowContainer>
      </React.Fragment>
    );
  }

  cancelNavigation() {
    this.setState({ overlayTileMenu: false });
  }

  showOverlayTileMenu() {
    this.setState({ overlayTileMenu: true });
  }

  cancelOverlayDialog() {
    this.setState({ dialogOverlay: false });
  }

  confirmOverlayDialog() {
    this.setState({ dialogOverlay: false });
  }

  showOverlayDialog() {
    this.setState({ dialogOverlay: true });
  }

  showBusyOverlay() {
    this.setState({ busyOverlay: true });
    setTimeout(() => { this.setState({ busyOverlay: false }) }, 3000);
  }
}
