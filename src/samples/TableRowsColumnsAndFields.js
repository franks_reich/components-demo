import React from 'react';

import {
  IconText, Table, Header, HeaderRow, Row, Field, PaddingContainer, RowContainer, ComponentTitle
} from 'franks-reich-components';


export const TableRowsColumnsAndFields = () => (
  <div>
    <RowContainer>
      <ComponentTitle>Table, Rows, Columns and Fields</ComponentTitle>
    </RowContainer>
    <PaddingContainer>
      <Table>
        <tbody>
          <HeaderRow>
            <Header colSpan='5'>Overall Header</Header>
          </HeaderRow>
          <HeaderRow>
            <Header>Table Header</Header>
            <Header>Table Header</Header>
            <Header>Table Header</Header>
            <Header>Table Header</Header>
            <Header>Table Header</Header>
          </HeaderRow>
          <Row>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field><IconText className='fas fa-thumbs-up' /></Field>
          </Row>
          <Row>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field><IconText className='fas fa-thumbs-up' /></Field>
          </Row>
          <Row>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field><IconText className='fas fa-thumbs-up' /></Field>
          </Row>
          <Row>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field>Table Field</Field>
            <Field><IconText className='fas fa-thumbs-up' /></Field>
          </Row>
        </tbody>
      </Table>
    </PaddingContainer>
  </div>
);
