import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import { ApplicationHeader } from 'franks-reich-components';
import { TextIconsAndButtons } from "./samples/TextIconsAndButtons";
import { TableOfContents } from "./table_of_contents/TableOfContents";
import { TableRowsColumnsAndFields } from "./samples/TableRowsColumnsAndFields";
import { TextInputsComboBoxesAndCheckBoxes } from "./samples/TextInputsComboBoxesAndCheckBoxes";
import { TileGroups } from "./samples/TileGroups";
import { OverlaysAndDialogs } from "./samples/OverlaysAndDialogs";
import { MarkdownAndToC } from "./samples/MarkdownAndToC";
import { PanelsAndCarousels } from "./samples/PanelsAndCarousels";


class App extends Component {
  render() {
    return (
      <Router basename={ process.env.PUBLIC_URL }>
        <div className="App">
          <ApplicationHeader
            title="Frank's Reich Components Showcase"
            menu={[
              {
                type: "Button",
                title: "Menu Button 1",
                onClick: () => { console.log("Clicked Menu Item 1") }
              },
              {
                type: "Button",
                title: "Menu Button 2",
                onClick: () => { console.log("Clicked Menu Item 2") }
              },
              {
                type: "Navigation",
                title: "Menu Navigation 1",
                navType: "Router",
                url: "/"
              },
              {
                type: "Menu",
                title: "Menu Menu 1",
                items: [
                  {
                    type: "Button",
                    title: "Sub Menu Button 1",
                    onClick: () => { console.log("Clicked Sub-Menu Button 1") }
                  },
                  {
                    type: "Button",
                    title: "Sub Menu Button 2",
                    onClick: () => { console.log("Clicked Sub-Menu Button 2") }
                  },
                  {
                    type: "Navigation",
                    title: "Sub Menu Navigation 1",
                    navType: "Router",
                    url: "/"
                  }
                ]
              }
            ]}
            navType="Router"
            homeUrl="/"
            fixed={ true } />
          <Route
            exact
            path='/'
            component={ TableOfContents } />
          <Route
            exact
            path='/texts_icons_and_buttons'
            component={ TextIconsAndButtons } />
          <Route
            exact
            path='/table_rows_columns_and_fields'
            component={ TableRowsColumnsAndFields } />
          <Route
            exact
            path='/text_inputs_combo_boxes_and_check_boxes'
            component={ TextInputsComboBoxesAndCheckBoxes } />
          <Route
            exact
            path='/tile_groups'
            component={ TileGroups } />
          <Route
            exact
            path='/overlays_and_dialogs'
            component={ OverlaysAndDialogs } />
          <Route
            exact
            path='/markdown_and_toc'
            component={ MarkdownAndToC } />
          <Route
            exact
            path='/panels_and_carousels'
            component={ PanelsAndCarousels } />
        </div>
      </Router>
    );
  }
}

export default App;
