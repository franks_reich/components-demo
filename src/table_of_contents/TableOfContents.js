import React from 'react';
import { Link } from 'react-router-dom';

import { RowContainer, ComponentTitle, Text } from 'franks-reich-components';


export const TableOfContents = () => (
  <div>
    <RowContainer>
      <ComponentTitle>Table of Contents</ComponentTitle>
    </RowContainer>
    <RowContainer>
      <Link to='/texts_icons_and_buttons'>
        <Text>1) Texts, Icons and Buttons</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/table_rows_columns_and_fields'>
        <Text>2) Table, Rows, Columns and Fields</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/text_inputs_combo_boxes_and_check_boxes'>
        <Text>3) Text Inputs, Combo Boxes and Check Boxes</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/tile_groups'>
        <Text>4) Tile Groups</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/overlays_and_dialogs'>
        <Text>5) Overlay</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/markdown_and_toc'>
        <Text>6) Markdown and ToC</Text>
      </Link>
    </RowContainer>
    <RowContainer>
      <Link to='/panels_and_carousels'>
        <Text>7) Panels and Carousels</Text>
      </Link>
    </RowContainer>
  </div>
);
